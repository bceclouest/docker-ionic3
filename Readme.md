
### ionic-v3.1.1

- Ubuntu 16.04 (Trusty)
- Java 8
- Node 8.0.0
- NPM 5.0.1
- Ionic 3.3.0
- Cordova 7.0.1
- Sass v3.4.24
